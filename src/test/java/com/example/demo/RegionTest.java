package com.example.demo;

import com.example.demo.services.RegionService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.Assert;

import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class RegionTest {

    @Autowired
    private RegionService regionService;

    public void setRegionService(RegionService regionService) {
        this.regionService = regionService;
    }

    @Test
    void getRegionsTest(){
        List<Integer> regions = regionService.getRegions();
        Assert.notEmpty(regions, "No existen regiones.");
    }
}
