package com.example.demo;

import com.example.demo.services.DrugStoreService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.Assert;

import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class DrugStoreTest {

    @Autowired
    private DrugStoreService drugStoreService;

    public void setDrugStoreService(DrugStoreService drugStoreService) {
        this.drugStoreService = drugStoreService;
    }

    @Test
    void getRegionDrugStoreTest(){
        List<?> drugStores = drugStoreService.getDrugStoresRegion(7);
        Assert.notEmpty(drugStores, "No existen farmacias para esta region.");
    }
}
