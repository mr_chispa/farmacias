package com.example.demo.rest;

import com.example.demo.services.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Clase encargada del manejo de requerimiento http asociado a
 * ubicacion geografica.
 *
 * @author Rodolfo Ortiz.
 */
@RestController
public class RegionController {

    @Autowired
    private RegionService regionService;

    public void setRegionService(RegionService regionService) {
        this.regionService = regionService;
    }

    /**
     * Metodo http que retorna regiones disponibles.
     * @return
     */
    @CrossOrigin
    @GetMapping(path = "/getRegions")
    public List<Integer> getRegions(){
        List<Integer> regions = regionService.getRegions();
        return regions;
    }

    /**
     * Metodo que retorna comunas para una region.
     * @param idRegion Identificador de la region.
     * @return
     */
    @CrossOrigin
    @GetMapping(path = "/getCommunes")
    public String getCommunes(@RequestParam Integer idRegion){
        String communes = regionService.getRegionDrugStores(idRegion);
        return communes;
    }

}
