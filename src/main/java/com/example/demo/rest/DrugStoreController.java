package com.example.demo.rest;

import com.example.demo.entities.DrugStoreEntity;
import com.example.demo.services.DrugStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Clase encargada de manejar requerimientos http
 * asociados a farmacias.
 * @author  Rodolfo Ortiz.
 */
@RestController
public class DrugStoreController {

    @Autowired
    private DrugStoreService drugStoreService;

    public void setDrugStoreService(DrugStoreService drugStoreService) {
        this.drugStoreService = drugStoreService;
    }

    /**
     * Metodo get encargado de retornar una lista de entidades
     * farmacioas para una region particular.
     * @param map
     * @return
     */
    @CrossOrigin
    @GetMapping(path = "getDrugStores")
    public List<DrugStoreEntity> getDrugStoresRegion(@RequestParam Map<String, String> map){
        Integer idRegion = Integer.parseInt(map.get("id_region"));
        List<DrugStoreEntity> drugStores = drugStoreService.getDrugStoresRegion(idRegion);
        return drugStores;
    }
}
