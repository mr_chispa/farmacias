package com.example.demo.services;

import com.example.demo.entities.CommuneEntity;

import java.util.List;

public interface RegionService {

    List<Integer> getRegions();
    String getRegionDrugStores(Integer idRegion);
}
