package com.example.demo.services;

import com.example.demo.entities.DrugStoreEntity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * Clase encargada de dar servicio de farmacias en funcion de terceros.
 * @author Rodolfo Ortiz.
 */
@Service("DrugStoreService")
public class DrugStoreServiceImpl implements DrugStoreService {

    /**
     * Metodo que retorna una lista de farmacias para una region dada.
     * @param idRegion Identificador de region.
     * @return
     */
    @Override
    public List<DrugStoreEntity> getDrugStoresRegion(Integer idRegion) {
        List<DrugStoreEntity>  communes = new ArrayList<>();
        String output = "";
        StringBuffer sb = new StringBuffer();

        try {

            URL url = new URL("https://farmanet.minsal.cl/maps/index.php/ws/getLocalesRegion");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("GET");

            DataOutputStream out = new DataOutputStream(conn.getOutputStream());
            out.writeBytes("id_region="+idRegion);
            out.flush();
            out.close();

            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                sb.append(output);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonToDrugStores(sb.toString());
    }

    /**
     * Metodo que convierte un json generado por terceros a entidades farmacias de aplicativo.
     * @param json
     * @return
     */
    private List<DrugStoreEntity> jsonToDrugStores(String json){
        ObjectMapper mapper = new ObjectMapper();
        List<DrugStoreEntity> drugStores = new ArrayList<>();
        try {
            drugStores = Arrays.asList(mapper.readValue(json, DrugStoreEntity[].class));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return drugStores;
    }
}
