package com.example.demo.services;

import com.example.demo.entities.DrugStoreEntity;

import java.util.List;

public interface DrugStoreService {

    /**
     * Servicio para obtener farmacias de una region.
     * @param idRegion
     * @return
     */
    List<DrugStoreEntity> getDrugStoresRegion(Integer idRegion);

}
