package com.example.demo.services;

import com.example.demo.entities.CommuneEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Clase encargada de dar servicio de regiones
 */
@Service("RegionService")
public class RegionServiceImpl implements RegionService {

    String lineEnd = "\r\n";
    private String twoHyphens = "--";
    private String boundary =  "*****";

    /**
     * Genera un rango de id regiones desde 1 a 16.
     * @return
     */
    @Override
    public List<Integer> getRegions() {
        List<Integer> regions = IntStream.range(1, 17)
                .boxed().collect(Collectors.toList());
        return regions;
    }

    @Override
    public String getRegionDrugStores(Integer idRegion) {

        List<CommuneEntity>  communes = new ArrayList<>();
        String output = "";
        StringBuffer sb = new StringBuffer();

        try {

            URL url = new URL("https://midastest.minsal.cl/farmacias/maps/index.php/utilidades/maps_obtener_comunas_por_regiones");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

            DataOutputStream outputStream = new DataOutputStream(conn.getOutputStream());
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);
            outputStream.writeBytes("Content-Disposition: form-data; name=\"reg_id\" ");
            outputStream.writeBytes("Content-Type: text/plain" + lineEnd);
            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(idRegion.toString());
            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                sb.append(output);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

}
